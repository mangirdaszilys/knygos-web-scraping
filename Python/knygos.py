#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from bs4 import BeautifulSoup
import datetime
import requests
import sys
import os
import re

print("PRADZIA")
pradzia=datetime.datetime.now()

dir="/Users/mz/Documents/Developer/Knygos/Python/"
df=pd.read_csv(dir+"Knygos.csv")
tmp=pd.read_csv(dir+"Parametrai.csv")
#df=df.sort_values(["URL"],ascending=[True]).reset_index(drop=True)

def url_isskyrimas(x):                                      #is URL isskirti full domain name (pvz: google.lt, knygos.lt)
    x=re.compile(r".*\.(lt|com|de|fr|info|org|co.uk)").match(x).group()
    return re.sub("(http://www.)|(https://www.)|(http://)|(https://)|(www.)","",x)

def sriuba(i):                                              #nueiti i url ir nuskaityti visa html faila
    page = requests.get(df.loc[i,"URL"])
    return BeautifulSoup(page.content,'html.parser')

def scrapeAutPav(soup,p):                                   #is html failo pagal parametrus istraukti formatuota teksta (autoriu ar pavadinima)
    x=soup.find(p[0],{p[1]:p[2]})
    if x: return x.get_text().strip()
    else: return ""

def scrapeKaina(soup,p):                                   #is html failo pagal parametrus istraukti kaina ir padauginti is nuolaidos koef
    x=soup.find(p[0],{p[1]:p[2]})
    if x: return round(float(re.sub("\\*.*", "", re.sub("[A-z,€,\n,\t,:space:, ]", "", re.sub(",", ".",x.get_text()))))*p[3],2)
    else: return ""

def priskirtiParametrus(i):                               #priskirti parametrus pagal einama eilute
    try: return tmp.loc[tmp.Website.isin([df.Website[i]])].fillna("").iloc[0]
    except: return [""]

def progresas(i):
    sys.stdout.write('\rProgresas: ' + str(i) + ' of ' + str(df.shape[0]))
    sys.stdout.flush()

df.Siandien=""
laikas=list(range(df.shape[0]))
for i in range(df.shape[0]):
    progresas(i+1)
    #pr=datetime.datetime.now()
    if pd.isnull(df.URL[i]): df.loc[i,"URL"]=df.loc[i,"Website"]="XXX"
    if pd.isnull(df.Website[i]): df.loc[i,"Website"]=url_isskyrimas(df.URL[i])
    parametrai=priskirtiParametrus(i)
    if (parametrai[0]==df.Website[i]):
        soup=sriuba(i)
        if pd.isnull(df.Autorius[i]): df.loc[i,"Autorius"]=scrapeAutPav(soup,parametrai[1:4])
        if pd.isnull(df.Pavadinimas[i]): df.loc[i,"Pavadinimas"]=scrapeAutPav(soup,parametrai[4:7])
        df.loc[i,"Siandien"]=scrapeKaina(soup,parametrai[7:11])
    #pb=datetime.datetime.now()
    #laikas[i]=round((pb-pr).seconds+(pb-pr).microseconds/1000000,6)

df.insert(df.shape[1],datetime.datetime.now().strftime("%Y-%m-%d"),df.Siandien,allow_duplicates=True)
df.Target=df.loc[:,"Siandien":].min(axis=1)
df=df.sort_values(["Autorius","Pavadinimas","Siandien"],ascending=[True,True,True])
df.to_csv(dir+"Knygos.csv",index=False)

pabaiga=datetime.datetime.now()
print("Uztruko: ",pabaiga-pradzia)
