#!/usr/bin/env Rscript

start_time <- Sys.time()

library(xml2)
library(rvest)
#library(XML)
library(httr)
library(RCurl)
library(readr)


#------------------------------------------------------------------------
infoURL<-function(i,temp_url_arg){
    html_nodes(read_html(html_session(paste(DB$URL[i]))),temp_url_arg) %>% html_text()
}
#------------------------------------------------------------------------
formatavimasAutPav<-function(z){
	gsub("(( *$)|(^ *))", "",gsub("(\\n|\\t| {2})", "",z))
}
#------------------------------------------------------------------------
formatavimasKaina<-function(z){
	return(as.numeric(gsub("\\*.*", "", gsub("[A-z,€,\n,\t,:space:, ]", "", gsub(",", ".",z)))))
}
#------------------------------------------------------------------------
formatavimas<-function(y1,y2,y3){
	return(c(formatavimasAutPav(y1),formatavimasAutPav(y2),formatavimasKaina(y3)))
}
#------------------------------------------------------------------------
nuskaitytiURL<-function(i){
x<-NULL
	if (DB$Website[i]=="humanitas.lt"){
		x<-infoURL(i,".book_author a , #the_price , h1 a")
		x<-formatavimas(x[2],x[1],x[3])
	}
	if (DB$Website[i]=="patogupirkti.lt"){
        x<-infoURL(i,"h2.font-22, a.link-underline, span.font-16")
		x<-formatavimas(x[2],x[1],x[3])
	}
	if (DB$Website[i]=="knyguklubas.lt"){
        x<-infoURL(i,".product-info-price .special-price .price , .product-info-main .product-item-author , .base")
		x<-formatavimas(x[2],x[1],x[3])
		x[3]=round(as.numeric(x[3])*0.85,2)
	}
	if (DB$Website[i]=="knygos.lt"){
        x<-infoURL(i,".bookTitle span, .price-block .primary , .book_details:nth-child(1) a")
		x<-formatavimas(x[3],x[1],x[2])
	}
	if (DB$Website[i]=="pegasas.lt"){
        x<-infoURL(i,"#product-info strong , #product-info .author a , h1")
		x<-formatavimas(x[2],x[1],x[3])
	}
	if (DB$Website[i]=="kitosknygos.lt"){
        x<-infoURL(i,".kaina span , .pavadinimas , .autorius")
		x<-formatavimas(x[1],x[2],x[3])
	}
	if (DB$Website[i]=="sofoklis.lt"){
        x<-infoURL(i,"#price , .vv .db span , #heading1 span")
		x<-formatavimas(x[3],x[1],x[2])
		x[3]=as.numeric(x[3])/100
	}
	if (DB$Website[i]=="tytoalba.lt"){
        x<-infoURL(i,"#middle_blocks .a1 span , #heading1 span , #price")
		x<-formatavimas(x[3],x[1],x[2])
		x[3]=as.numeric(x[3])/100
	}
	if (DB$Website[i]=="johnlewis.com"){
        x<-infoURL(i,".product-header__title , .price--large")
		#x<-c(x[2],x[1],x[3])
	}
	if (DB$Website[i]=="bookdepository.com"){
        x<-infoURL(i,"span:nth-child(1) span , h1 , .sale-price")
		x<-formatavimas(x[2],x[1],x[3])
	}
	if (DB$Website[i]=="amazon.de"){
        x<-infoURL(i,"span.a-size-base.a-color-price")
		#x<-c(x[2],x[1],x[3])
	}
	return(x)
}
#------------------------------------------------------------------------
puslapis<-function(i){
    naujas<-regmatches(DB$URL[i],regexpr(".*\\.(lt|com|de|fr|info|org|co\\.uk)",DB$URL[i]))
    gsub("(http://www.)|(https://www.)|(http://)|(https://)|(www.)","",naujas)
}
#------------------------------------------------------------------------

DB<-NULL

failas="Knygos3.csv"
DB<-read.csv(file=failas,header=TRUE,stringsAsFactors = FALSE,check.names = FALSE)
for (i in 1:nrow(DB)){
	DB$Website[i]<-c(puslapis(i))
    if (url.exists(DB$URL[i])){
    	UrlDB<-nuskaitytiURL(i)
    	if (DB$Autorius[i]==""){
    		DB$Autorius[i]<-c(UrlDB[1])
    	}
    	if (DB$Pavadinimas[i]==""){
    		DB$Pavadinimas[i]<-c(UrlDB[2])  
    	}
    	DB$SndKaina[i]<-c(UrlDB[3])
	}
	else {
		DB$SndKaina[i]<-c("NA")
		DB$Website[i]<-c("NA")
		DB$Autorius[i]<-c("NA")
		DB$Pavadinimas[i]<-c("NA")
	    }
}
DB$Target<-c(apply(DB[,8:ncol(DB)],1,min,na.rm = TRUE))
DB$Siandien<-DB$SndKaina

DB<-DB[order(DB[,4], DB[,5]), ]

colnames(DB)[colnames(DB)=="SndKaina"] <- strftime(Sys.Date(),"%Y-%m-%d")
write.csv(DB,file=failas,row.names = FALSE)

proc.time()
end_time <- Sys.time()
end_time - start_time

